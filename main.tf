locals {
  tags = merge(
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-terraform-baseline"
    },
    var.tags
  )
}

#####
# Terraform protection policy
#####

resource "aws_iam_policy" "terraform_protection" {
  count = var.iam_policy_terraform_protection_enable ? 1 : 0

  name   = format("%s%s", var.prefix, var.iam_policy_terraform_protection_name)
  path   = "/"
  policy = data.aws_iam_policy_document.terraform_protection.json
  tags   = local.tags
}
