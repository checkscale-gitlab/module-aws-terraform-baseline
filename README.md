# Module AWS Terraform Baseline

Module to create baseline resources useful for Terraform.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.15 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.15 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.terraform_protection](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy_document.terraform_protection](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_iam_policy_terraform_protection_enable"></a> [iam\_policy\_terraform\_protection\_enable](#input\_iam\_policy\_terraform\_protection\_enable) | Whether ot not to create the IAM policy to protect all resources tagged `managed-by`=`terraform` of any write operation. | `bool` | `false` | no |
| <a name="input_iam_policy_terraform_protection_name"></a> [iam\_policy\_terraform\_protection\_name](#input\_iam\_policy\_terraform\_protection\_name) | Name of the policy to create for protecting all resources tagged `managed-by`=`terraform`. Ignored if `var.iam_policy_terraform_protection_enable` is `false`. | `string` | `"TerraformResourcesProtection"` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Prefix to be used for all resources names. Specifically useful for tests. | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be used in every resources created by this module. | `map` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_policy_policy_terraform_protection_id"></a> [iam\_policy\_policy\_terraform\_protection\_id](#output\_iam\_policy\_policy\_terraform\_protection\_id) | n/a |
| <a name="output_iam_policy_terraform_protection_arn"></a> [iam\_policy\_terraform\_protection\_arn](#output\_iam\_policy\_terraform\_protection\_arn) | n/a |
| <a name="output_iam_policy_terraform_protection_id"></a> [iam\_policy\_terraform\_protection\_id](#output\_iam\_policy\_terraform\_protection\_id) | n/a |
| <a name="output_iam_policy_terraform_protection_path"></a> [iam\_policy\_terraform\_protection\_path](#output\_iam\_policy\_terraform\_protection\_path) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.
