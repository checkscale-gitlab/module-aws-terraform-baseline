#####
# Default
#####

resource "random_string" "this" {
  length = 8

  special = false
}

module "disabled" {
  source = "../../"

  count = 0
}
