#####
# Terraform protection policy
#####

output "iam_policy_terraform_protection_id" {
  value = module.default.iam_policy_terraform_protection_id
}

output "iam_policy_terraform_protection_arn" {
  value = module.default.iam_policy_terraform_protection_arn
}

output "iam_policy_policy_terraform_protection_id" {
  value = module.default.iam_policy_policy_terraform_protection_id
}

output "iam_policy_terraform_protection_path" {
  value = module.default.iam_policy_terraform_protection_path
}
