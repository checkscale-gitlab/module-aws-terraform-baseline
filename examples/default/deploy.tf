#####
# Disabled
#####

resource "random_string" "default" {
  length = 8

  special = false
}

module "default" {
  source = "../../"

  prefix = random_string.default.result

  iam_policy_terraform_protection_enable = true
  iam_policy_terraform_protection_name   = "tftest"
}
